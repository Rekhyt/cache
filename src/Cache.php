<?php
namespace WeirdWebdesign\Cache;

interface Cache
{
    const EXP_HOURLY  = 3600;     // 60 minutes
    const EXP_DAILY   = 86400;     // 24 hours
    const EXP_WEEKLY  = 604800;   // 7 days
    const EXP_MONTHLY = 2419200; // 28 days or 7x4 days
    const EXP_YEARLY  = 31536000; // 365 days
    
    /**
     * @return string[] The list of identifiers currently stored in the cache.
     */
    public function getIdentifiers();
    
    /**
     * Tell if an entry with a given identifier exists.
     *
     * @param string $identifier The identifier to look up.
     *
     * @return bool True if an entry exists, false otherwise.
     */
    public function exists($identifier);
    
    /**
     * Read an entry from the cache.
     *
     * @param string $identifier The identifier of the entry to be fetched.
     *
     * @return mixed The entry found for the identifier or null if none was found.
     */
    public function read($identifier);
    
    /**
     * Insert or update an entry in(to) the cache.
     *
     * @param string $identifier       The identifier for the entry.
     * @param mixed  $entry            The entry itself.
     */
    public function update($identifier, $entry);
    
    /**
     * Delete an entry from the cache.
     *
     * @param string $identifier The identifier for the entry that should be deleted from the cache.
     */
    public function delete($identifier);
    
    /**
     * Forces the cache to expire.
     */
    public function invalidate();
    
    /**
     * Saves the current cache content.
     */
    public function persist();
    
    /**
     * Deletes all data in the cache and deletes the storage.
     */
    public function purge();
}