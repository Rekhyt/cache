<?php
namespace WeirdWebdesign\Cache\Cache\Basic;

use DateTime;
use DateTimeZone;
use WeirdWebdesign\Cache\Cache\Basic;
use WeirdWebdesign\Cache\Storage;

class FixedIntervalExpiry extends Basic
{
    /** @var int */
    protected $startAt;
    
    /**
     * @param Storage $storage
     * @param int     $interval in seconds, defaults to after 24 hours, must be > 0
     * @param int     $startAt  timestamp of first expiry, defaults to 0
     */
    public function __construct(Storage $storage, $interval = self::EXP_DAILY, $startAt = 0)
    {
        $this->startAt = $startAt;
        
        parent::__construct($storage, $interval);
    }
    
    protected function calculateExpiryTimestamp($reference = null)
    {
        $reference = ($reference === null)
            ? new DateTime('now', new DateTimeZone('UTC'))
            : new DateTime('@' . $reference);
        
        $diffToStartTime = $reference->getTimestamp() - $this->startAt;
        
        // we didn't even reach the starting time, yet
        if ($diffToStartTime < 0) {
            return $this->startAt;
        }
        
        return ((int)($diffToStartTime / $this->expiration)) * $this->expiration + $this->expiration;
        
    }
}