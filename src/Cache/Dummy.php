<?php
namespace WeirdWebdesign\Cache\Cache;

use WeirdWebdesign\Cache\Cache;

/**
 * An empty "dummy" implementation of Cache doing nothing.
 *
 * Use this to avoid "is_null()" checks throughout the code when no cache is needed.
 */
class Dummy implements Cache
{
    public function getIdentifiers()
    {
        return array();
    }
    
    public function exists($identifier)
    {
        return true;
    }
    
    public function read($identifier)
    {
        return null;
    }
    
    public function update($identifier, $entry)
    {
    }
    
    public function delete($identifier)
    {
    }
    
    public function invalidate()
    {
    }
    
    public function persist()
    {
    }
    
    public function purge()
    {
    }
}