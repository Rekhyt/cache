<?php
namespace WeirdWebdesign\Cache\Cache;

use DateTime;
use DateTimeZone;
use WeirdWebdesign\Cache\Cache;
use WeirdWebdesign\Cache\Storage;

/**
 * A Cache with expiry time and support for different storages.
 */
class Basic implements Cache
{
    /**
     * @var Storage
     */
    protected $storage;
    
    /**
     * @var int time after which the cache is considered expired
     */
    protected $expiration;
    
    /**
     * @var bool true when the cache has expired
     */
    protected $expired;
    
    /**
     * @var bool true when a value in the cache has changed, false otherwise
     */
    protected $invalidated;
    
    /**
     * @param Storage $storage
     * @param int     $expiration defaults to after 24 hours
     */
    public function __construct(Storage $storage, $expiration = self::EXP_DAILY)
    {
        $this->storage     = $storage;
        $this->expiration  = $expiration;
        $this->expired     = $this->checkExpired();
        $this->invalidated = false;
    }
    
    public function getIdentifiers()
    {
        return $this->storage->getIdentifiers();
    }
    
    public function exists($identifier)
    {
        return $this->storage->exists($identifier);
    }
    
    public function read($identifier)
    {
        return $this->expired
            ? null
            : $this->storage->read($identifier);
    }
    
    public function update($identifier, $entry)
    {
        $this->storage->update($identifier, $entry);
        $this->invalidate();
    }
    
    public function delete($identifier)
    {
        $this->storage->delete($identifier);
    }
    
    public function invalidate()
    {
        $this->invalidated = true;
    }
    
    public function persist()
    {
        $expirationFromNow = $this->calculateExpiryTimestamp();
        $timestamp         = ($this->invalidated && $this->checkExpired($expirationFromNow))
            ? $expirationFromNow
            : $this->storage->getTimestamp();
        
        $this->storage->persist($timestamp);
    }
    
    public function purge()
    {
        $this->storage->purge();
    }
    
    /**
     * @param int $reference Timestamp from which to check whether the cache is expired or not. Pass null to use 'now'.
     *
     * @return bool
     */
    protected function checkExpired($reference = null)
    {
        $reference = ($reference === null)
            ? new DateTime('now', new DateTimeZone('UTC'))
            : new DateTime('@' . $reference);
        
        return ($reference->getTimestamp() > ($this->storage->getTimestamp()));
    }
    
    /**
     * @param int $reference Timestamp from which to calculate the new expiry time. Pass null to use 'now'.
     *
     * @return int
     */
    protected function calculateExpiryTimestamp($reference = null)
    {
        $reference = ($reference === null)
            ? new DateTime('now', new DateTimeZone('UTC'))
            : new DateTime('@' . $reference);
        
        return $reference->getTimestamp() + $this->expiration;
    }
}