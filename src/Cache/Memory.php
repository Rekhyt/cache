<?php
namespace WeirdWebdesign\Cache\Cache;

use WeirdWebdesign\Cache\SortableCache;

/**
 * A simple non-persistent SortableCache implementation that simply manages key-value pairs in the memory and will
 * always assume invalidated entries.
 */
class Memory implements SortableCache
{
    /**
     * @var array [string, mixed]
     */
    protected $storage;
    
    public function __construct()
    {
        $this->storage = array();
    }
    
    public function getIdentifiers()
    {
        return array_keys($this->storage);
    }
    
    public function exists($identifier)
    {
        return isset($this->storage[$identifier]);
    }
    
    public function read($identifier)
    {
        return $this->storage[$identifier];
    }
    
    public function update($identifier, $entry)
    {
        $this->storage[$identifier] = $entry;
    }
    
    public function delete($identifier)
    {
        unset($this->storage[$identifier]);
    }
    
    public function invalidate()
    {
        return; // always invalidated
    }
    
    public function persist()
    {
        return; // non-persitent
    }
    
    public function purge()
    {
        $this->storage = array();
    }
    
    public function sortBy($fieldName, $sortOrder = self::SORT_ORDER_ASC, $typeCast = self::TYPE_CAST_INT)
    {
        usort(
            $this->storage,
            function (array $resource1, array $resource2) use ($fieldName, $sortOrder, $typeCast) {
                switch ($typeCast) {
                    default:
                    case 'int':
                        $res1 = (int)$resource1[$fieldName];
                        $res2 = (int)$resource2[$fieldName];
                        break;
                    case 'float':
                        $res1 = (float)$resource1[$fieldName];
                        $res2 = (float)$resource2[$fieldName];
                        break;
                    case 'string':
                        $result = strnatcmp((string)$resource1[$fieldName], (string)$resource2[$fieldName]);
                        
                        return ($sortOrder == SortableCache::SORT_ORDER_ASC)
                            ? $result
                            : $result * -1;
                }
                
                $diff = $res1 - $res2;
                switch ($diff) {
                    case 0:
                        return 0;
                        break;
                    default:
                        $result = ($diff > 0) ? 1 : -1;
                        
                        return $sortOrder == SortableCache::SORT_ORDER_ASC
                            ? $result
                            : $result * -1;
                }
            }
        );
    }
}