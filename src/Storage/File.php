<?php
namespace WeirdWebdesign\Cache\Storage;

use Exception;
use WeirdWebdesign\Cache\Storage;

/**
 * A file-based cache using the JSON format.
 * 
 * Expiry timestamp is stored in the first line, actual cached data as a JSON object in the following line(s).
 */
class File implements Storage
{
    /**
     * @var string
     */
    protected $filePath;
    
    /**
     * @var resource
     */
    protected $fileHandle;
    
    /**
     * @var int
     */
    protected $timestamp;
    
    /**
     * @var array [string, mixed]
     */
    protected $storage;
    
    public function __construct($filePath)
    {
        $this->filePath = $filePath;
        $this->initialize();
    }
    
    public function getIdentifiers()
    {
        return array_combine(array_keys($this->storage), array_keys($this->storage));
    }
    
    public function exists($identifier)
    {
        return isset($this->storage[$identifier]);
    }
    
    public function read($identifier)
    {
        return (isset($this->storage[$identifier]))
            ? $this->storage[$identifier]
            : null;
    }
    
    public function update($identifier, $entry)
    {
        $this->storage[$identifier] = $entry;
    }
    
    public function delete($identifier)
    {
        if (isset($this->storage[$identifier])) {
            unset($this->storage[$identifier]);
        }
    }
    
    public function getTimestamp()
    {
        return $this->timestamp;
    }
    
    public function persist($timestamp = null)
    {
        if ($timestamp === null) {
            $timestamp = time();
        }
        
        $cacheContents = $timestamp . "\n" . @json_encode($this->storage);
        
        @fclose($this->fileHandle); // temporarily close file handle, restore at the end in case of success
        
        $result = @file_put_contents($this->filePath, $cacheContents);
        if ($result === false) {
            throw new Exception('Cannot write cache file at "' . $this->filePath . '".');
        }
        
        $this->openFileHandle();
    }
    
    public function purge()
    {
        @fclose($this->fileHandle);
        @unlink($this->filePath);
    }
    
    protected function initialize()
    {
        $this->openFileHandle();
        $this->initializeTimestamp();
        $this->initializeStorage();
    }
    
    protected function openFileHandle()
    {
        $this->fileHandle = @fopen($this->filePath, 'a+');
        if ($this->fileHandle === false) {
            throw new Exception('Cannot read cache file at "' . $this->filePath . '".');
        }
    }
    
    protected function initializeStorage()
    {
        if (feof($this->fileHandle) || (filesize($this->filePath) == 0)) {
            return;
        }
        
        $cachedData = @fread($this->fileHandle, filesize($this->filePath) - strlen($this->timestamp));
        
        if ($cachedData === false) {
            throw new Exception('Cannot read data from cache file at "' . $this->filePath . '".', true);
        }
        
        $this->storage = @json_decode($cachedData, true);
    }
    
    protected function initializeTimestamp()
    {
        if (rewind($this->fileHandle) === false) {
            throw new Exception('Cannot reset file pointer in cache file at "' . $this->filePath . '".', true);
        }
        
        $timestamp = @fgets($this->fileHandle);
        
        if (($timestamp === false) && !feof($this->fileHandle)) {
            throw new Exception('Cannot read timestamp from cache file at "' . $this->filePath . '".', true);
        }
        
        $this->timestamp = trim($timestamp);
    }
}