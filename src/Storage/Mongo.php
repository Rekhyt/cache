<?php
namespace WeirdWebdesign\Cache\Storage;

use MongoCollection;
use WeirdWebdesign\Cache\Storage;

class Mongo implements Storage
{
    const DEFAULT_MONGO_ID_FIELD_NAME = '_id';
    const TIMESTAMP_ID                = 'SHOPGATE_MONGO_CACHE_TIMESTAMP';
    
    /**
     * @var MongoCollection
     */
    protected $collection;
    
    /**
     * @var bool
     */
    protected $forceOverwriteEmpty;
    
    /**
     * @var string
     */
    protected $mongoIdFieldName;
    
    /**
     * @var array [string, string] A cached list of identifiers in the storage. The identifiers are the keys and the values.
     */
    protected $identifiers;
    
    /**
     * @param MongoCollection $collection          The collection object the cache is located in.
     * @param bool            $forceOverwriteEmpty True to force saving "null" values or empty arrays on updating the cache.
     * @param string          $mongoIdFieldName    The name of the field that is used as identifier in the collection.
     */
    public function __construct(
        MongoCollection $collection,
        $forceOverwriteEmpty = false,
        $mongoIdFieldName = self::DEFAULT_MONGO_ID_FIELD_NAME
    ) {
        $this->collection          = $collection;
        $this->forceOverwriteEmpty = $forceOverwriteEmpty;
        $this->mongoIdFieldName    = $mongoIdFieldName;
        $this->initializeIdentifiers();
    }
    
    public function getIdentifiers()
    {
        $this->initializeIdentifiers();
        
        return $this->identifiers;
    }
    
    public function exists($identifier)
    {
        return isset($this->identifiers[$identifier]);
    }
    
    public function read($identifier)
    {
        return $this->collection->findOne(array($this->mongoIdFieldName => $identifier));
    }
    
    public function update($identifier, $entry)
    {
        if (!$this->forceOverwriteEmpty) {
            $entry = array_filter($entry, array($this, 'arrayFilterNulls'));
        }
        
        $entry[$this->mongoIdFieldName] = $identifier;
        $this->collection->update(
            array($this->mongoIdFieldName => $identifier), array('$set' => $entry), array('upsert' => true)
        );
        
        if (!$this->exists($identifier)) {
            $this->identifiers[$identifier] = $identifier;
        }
    }
    
    public function delete($identifier)
    {
        $this->collection->remove(array($this->mongoIdFieldName => $identifier));
    }
    
    public function getTimestamp()
    {
        $result = $this->collection->findOne(array($this->mongoIdFieldName => self::TIMESTAMP_ID));
        
        return !empty($result['timestamp'])
            ? (int)$result['timestamp']
            : time() // return current time if the cache has just been created, i.e. no timestamp is available, yet
            ;
    }
    
    public function persist($timestamp = null)
    {
        if ($timestamp === null) {
            $timestamp = time();
        }
        
        $this->collection->update(
            array($this->mongoIdFieldName => self::TIMESTAMP_ID), array('timestamp' => $timestamp)
        );
    }
    
    public function purge()
    {
        $this->collection->drop();
    }
    
    /**
     * Initializes $this->identifiers unless it's not empty.
     */
    protected function initializeIdentifiers()
    {
        $this->identifiers = array();
        $cursor            = $this->collection->find(array(), array($this->mongoIdFieldName => true));
        $cursor->sort(array('_id' => 1));
        while ($cursor->hasNext()) {
            $cursor->next();
            $document                                              = $cursor->current();
            $this->identifiers[$document[$this->mongoIdFieldName]] = $document[$this->mongoIdFieldName];
        }
    }
    
    /**
     * Callback for array_filter finding "null" values or empty arrays.
     *
     * @param mixed $value The value to test for.
     *
     * @return bool True when the value is NOT null or an empty array.
     */
    protected function arrayFilterNulls($value)
    {
        return !is_null($value) && !(is_array($value) && empty($value));
    }
}