<?php
namespace WeirdWebdesign\Cache;

/**
 * Classes implementing this handle the loading and saving of a cache for a specific store.
 *
 * A store can be e.g. a file or a database.
 */
interface Storage
{
    /**
     * @return array [string, string] The identifiers currently available in the storage as keys AND values.
     */
    public function getIdentifiers();
    
    /**
     * Tell if an entry with a given identifier exists in the storage.
     *
     * @param string $identifier The identifier to look up.
     *
     * @return bool True if an entry for the identifier exists, false otherwise.
     */
    public function exists($identifier);
    
    /**
     * Read an entry from the cache storage.
     *
     * @param string $identifier The identifier to fetch the value for.
     *
     * @return mixed The entry found for the identifier or null if none was found.
     */
    public function read($identifier);
    
    /**
     * Insert or update an entry in(to) the cache storage.
     *
     * @param string $identifier The identifier for the entry.
     * @param mixed  $entry      The entry itself.
     */
    public function update($identifier, $entry);
    
    /**
     * Delete an entry from the cache storage.
     *
     * @param string $identifier The identifier for the entry that should be deleted from the cache storage.
     */
    public function delete($identifier);
    
    /**
     * Returns the timestamp of the last save.
     *
     * @return int
     */
    public function getTimestamp();
    
    /**
     * Persists the current cache state and timestamp to the storage.
     *
     * @param int $timestamp Timestamp for the time of saving, pass null to use the value of time().
     */
    public function persist($timestamp = null);
    
    /**
     * Deletes the cache storage.
     */
    public function purge();
}