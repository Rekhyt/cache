<?php
namespace WeirdWebdesign\Cache;

interface SortableCache extends Cache
{
    const SORT_ORDER_ASC  = 'asc';
    const SORT_ORDER_DESC = 'desc';
    
    const TYPE_CAST_INT    = 'int';
    const TYPE_CAST_FLOAT  = 'float';
    const TYPE_CAST_STRING = 'string';
    
    /**
     * Sorts the contents of the cache by a field name of the containing array.
     *
     * Sorts ascending only for now. Uses "mergesort".
     *
     * @param string $fieldName The name of the field in the containing array or null to use the default sort field.
     * @param string $sortOrder One of the SORT_ORDER_* constants.
     * @param string $typeCast  One of the TYPE_CAST_* constants.
     * @post $this contains a list of the same Resource objects as before, sorted ascending by the given field name.
     */
    public function sortBy($fieldName, $sortOrder = self::SORT_ORDER_ASC, $typeCast = self::TYPE_CAST_INT);
}