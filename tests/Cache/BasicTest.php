<?php
namespace WeirdWebdesign\Tests\Cache\Cache;

use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;
use WeirdWebdesign\Cache\Cache\Basic;
use WeirdWebdesign\Cache\Storage;

class BasicTest extends PHPUnit_Framework_TestCase
{
    /** @var Storage|PHPUnit_Framework_MockObject_MockObject */
    protected $storage;
    
    /** @var Basic */
    protected $cache;
    
    public function setUp()
    {
        $this->storage = $this->getMockForAbstractClass('WeirdWebdesign\Cache\Storage');
        $this->cache   = new Basic($this->storage);
    }
    
    public function tearDown()
    {
        unset($this->storage);
        unset($this->cache);
    }
    
    public function testGetIdentifiers() {
        $this->storage->expects($this->once())->method('getIdentifiers');
        $this->cache->getIdentifiers();
    }
}